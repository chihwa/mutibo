package com.mutibo.austin.client;

import java.util.Collection;
import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Chihwa on 11/9/2014.
 */
public interface MutiboSetAPI {


    //sync
    @GET("/login")
    public Boolean getLogin();

    //async
    @GET("/set")
    public void getSets(Callback<List<MutiboSet>> response);

    @GET("/set/{id}")
    public void getSetForId(@Path("id") String id, Callback<MutiboSet> ms);

    @POST("/set")
    public void addSet(@Body MutiboSet v, Callback<MutiboSet> ms);

    @POST("/set/{id}")
    public void presentedForId(@Query("obscure") String param1, @Path("id") String id, Callback<MutiboSet> ms);
}
