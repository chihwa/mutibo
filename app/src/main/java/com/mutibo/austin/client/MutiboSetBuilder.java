package com.mutibo.austin.client;

public class MutiboSetBuilder {
    private String id;
    private String question;
    private String choice;
    private int answer;
    private String explain;
    private int presentedCnt;
    private int issueCnt;

    public MutiboSetBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public MutiboSetBuilder setQuestion(String question) {
        this.question = question;
        return this;
    }

    public MutiboSetBuilder setChoice(String choice) {
        this.choice = choice;
        return this;
    }

    public MutiboSetBuilder setAnswer(int answer) {
        this.answer = answer;
        return this;
    }

    public MutiboSetBuilder setExplain(String explain) {
        this.explain = explain;
        return this;
    }

    public MutiboSetBuilder setPresentedCnt(int presentedCnt) {
        this.presentedCnt = presentedCnt;
        return this;
    }

    public MutiboSetBuilder setIssueCnt(int issueCnt) {
        this.issueCnt = issueCnt;
        return this;
    }

    public MutiboSet createMutiboSet() {
        return new MutiboSet(id, question, choice, answer, explain, presentedCnt, issueCnt);
    }
}