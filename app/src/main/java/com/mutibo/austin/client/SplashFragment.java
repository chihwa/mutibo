package com.mutibo.austin.client;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.mutibo.austin.client.oauth.SecuredRestException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Chihwa on 11/10/2014.
 */
public class SplashFragment extends Fragment {

    @InjectView(R.id.userName)
    protected EditText userName_;

    @InjectView(R.id.password)
    protected EditText password_;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.splash,
                container, false);

        ButterKnife.inject(this, view);

        return view;
    }

    @OnClick(R.id.loginButton)
    public void login() {
        String user = userName_.getText().toString();
        String pass = password_.getText().toString();

        MutiboSvc.user = user;
        MutiboSvc.pass = pass;

        final MutiboSetAPI svc = MutiboSvc.init(user, pass);

        new Thread(new Runnable() {
            public void run() {
                Boolean loginReturn = false;
                try {
                    loginReturn = svc.getLogin();
                } catch (Exception e) {
                    Log.e("LOGIN", "Login is failure with Exception");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(
                                    getActivity(),
                                    "Login failed, check your Internet connection and credentials.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                if (loginReturn) {
                    Log.e("LOGIN", "Login is success");
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.putExtra("MUTIBO_LOGIN", Boolean.TRUE);
                    startActivity(i);
                }
            }
        }).start();
    }
}
