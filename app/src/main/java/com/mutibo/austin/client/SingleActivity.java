package com.mutibo.austin.client;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import org.apache.http.impl.client.DefaultHttpClient;

import java.util.List;
import java.util.Random;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SingleActivity extends Activity {

    //for Genymotion            "https://10.0.3.2:8443";
    private MutiboSet mset;
    private List<MutiboSet> msets;
    private int right = 0;
    private int wrong = 0;
    private Random r = new Random();

    private static final String TAG = "MainActivity";


    @Override
    public void onStop() {
        super.onStop();
        stopService(new Intent(getBaseContext(), MusicService.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }



        //fetch a question from server and attach it to the single fragment
        requestData();
    }

    @Override
    public void onResume() {
        super.onResume();
        populateScore();
        populateLife();
        //submit button is deactivated till question is loaded from server
        (findViewById(R.id.submitnext)).setActivated(false);
        (findViewById(R.id.obscureCheck)).setEnabled(false);
        startService(new Intent(this, MusicService.class));
    }

    private void getNextRandomSet() {
        int length = msets.size();
        if (0 == length) {
            mset = new MutiboSetBuilder().createMutiboSet();
            mset.setQuestion("There's no MutiboSet at server");
            mset.setAnswer(4); //user cannot have this question right
            mset.setExplain("Dummy explanation");
            mset.setChoice("Dummy Choice 1;Dummy Choice 2;Dummy Choice 3;Dummy Choice 4");
            mset.setId("0");
            mset.setIssueCnt(0);
            mset.setPresentedCnt(0);
        } else {
            int randomIndex = r.nextInt(length);
            mset = msets.get(randomIndex);
            msets.remove(randomIndex);
        }
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    private void requestData() {
        DefaultHttpClient ehc = new EasyHttpClient();

        final MutiboSetAPI api = MutiboSvc.getOrShowLogin(this);

        if (null != api) {
            new Thread(new Runnable() {
                public void run() {
                    api.getSets(new Callback<List<MutiboSet>>() {
                        @Override
                        public void success(List<MutiboSet> mutiboSets, Response response) {
                            Log.d("mutibo", mutiboSets.toString());
                            msets = mutiboSets;

                            getNextRandomSet();

                            populateQuestion();
                            populateSelection();
                            //activate submit button after question is loaded
                            ((Button) findViewById(R.id.submitnext)).setActivated(true);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            System.out.println("failed=" + error);
                        }
                    });
                }
            }).start();
        }
    }


    private void populateSelection() {
        String[] choices = mset.getChoice().split(";");
        ((RadioButton) findViewById(R.id.select1)).setText(choices[0]);
        ((RadioButton) findViewById(R.id.select2)).setText(choices[1]);
        ((RadioButton) findViewById(R.id.select3)).setText(choices[2]);
        ((RadioButton) findViewById(R.id.select4)).setText(choices[3]);
    }

    private void cleanSelection() {
        ((RadioGroup) findViewById(R.id.select)).clearCheck();
    }

    private void populateQuestion() {
        ((TextView) findViewById(R.id.question)).setText(mset.getQuestion() + "(" + mset.getId() + ")");
    }

    private void populateLife() {
        ((RatingBar) findViewById(R.id.life)).setRating(Float.valueOf(3 - wrong));
    }

    private void populateScore() {
        String scoreStr = "Score " + String.valueOf(right) + "/" + String.valueOf(right + wrong);
        ((TextView) findViewById(R.id.score)).setText(scoreStr);
    }

    //click submit or next
    public void submitClicked(View view) {
        String currentText = ((Button) view).getText().toString();

        if (currentText.equals("Submit")) {
            enableRadioButtons(false);

            RadioGroup radioButtonGroup = ((RadioGroup) findViewById(R.id.select));
            int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
            View radioButton = radioButtonGroup.findViewById(radioButtonID);
            int idx = radioButtonGroup.indexOfChild(radioButton);

            StringBuilder explain = new StringBuilder(mset.getExplain());
            if (idx == mset.getAnswer()) {
                right++;
                explain.insert(0, "That's right. ");
            } else {
                wrong++;
                explain.insert(0, "That's wrong. ");
            }
            populateScore();
            populateLife();
            ((TextView) findViewById(R.id.explain)).setText(explain);

            ((Button) view).setText("Next");


            if (wrong > 2) {

                Button newPage = (Button) view.findViewById(R.id.submitnext);
                newPage.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        evaluateSet();
                        Intent intent = new Intent(v.getContext(), GameOverActivity.class);
                        finish();
                        intent.putExtra("score", String.valueOf(right));
                        startActivity(intent);
                    }
                });
            }

            ((CheckBox) findViewById(R.id.obscureCheck)).setEnabled(true);

        } else if (currentText.equals("Next")) {


            requestData();  //question and selection are loaded

            ((Button) view).setText("Submit");
            ((TextView) findViewById(R.id.explain)).setText("");
            cleanSelection();
            enableRadioButtons(true);

            evaluateSet();

            CheckBox cb = ((CheckBox) findViewById(R.id.obscureCheck));
            cb.setChecked(false);
            cb.setEnabled(false);
        }

    }

    private void evaluateSet() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(MutiboSvc.ENDPOINT)
                .build();
        MutiboSetAPI api = adapter.create(MutiboSetAPI.class);

        String obscureString = ((CheckBox) findViewById(R.id.obscureCheck)).isChecked() ? "true" : "false";

        api.presentedForId(obscureString, mset.getId(), new Callback<MutiboSet>() {
            @Override
            public void success(MutiboSet mutiboSet, Response response) {
                Log.d("updatedSet=", mutiboSet.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("evaluateSet failed=", error.toString());

            }
        });
    }

    private void enableRadioButtons(boolean flag) {
        ((RadioButton) findViewById(R.id.select1)).setEnabled(flag);
        ((RadioButton) findViewById(R.id.select2)).setEnabled(flag);
        ((RadioButton) findViewById(R.id.select3)).setEnabled(flag);
        ((RadioButton) findViewById(R.id.select4)).setEnabled(flag);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_single, container, false);
            return rootView;
        }
    }
}
