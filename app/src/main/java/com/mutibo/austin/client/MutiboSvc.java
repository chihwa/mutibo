package com.mutibo.austin.client;

import android.content.Context;
import android.content.Intent;

import com.mutibo.austin.client.oauth.SecuredRestBuilder;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

public class MutiboSvc {

    //public static final String ENDPOINT = "https://192.168.1.110:8443";
    public static final String ENDPOINT = "https://54.149.249.254:8443";
    public static final String TOKEN_PATH = "/oauth/token";
    public static String user = "admin";
    public static String pass = "pass";
    public static final String CLIENT_ID = "mobile";

    private static MutiboSetAPI mutiboSvc_;

    public static synchronized MutiboSetAPI getOrShowLogin(Context ctx) {
        if (mutiboSvc_ != null) {
            return mutiboSvc_;
        } else {
            //show splash fragment
            Intent i = new Intent(ctx, MainActivity.class);
            ctx.startActivity(i);
            return null;
        }
    }

    public static synchronized MutiboSetAPI init(String user,
                                                 String pass) {

        String server = MutiboSvc.ENDPOINT;
        mutiboSvc_ = new SecuredRestBuilder()
                .setLoginEndpoint(server + MutiboSvc.TOKEN_PATH)
                .setUsername(user)
                .setPassword(pass)
                .setClientId(CLIENT_ID)
                .setClient(
                        new ApacheClient(new EasyHttpClient()))
                .setEndpoint(server).setLogLevel(LogLevel.FULL).build()
                .create(MutiboSetAPI.class);

        return mutiboSvc_;
    }
}
