package com.mutibo.austin.client;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class MusicService extends Service {
    MediaPlayer mp;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        //play music here
        new Thread(new Play()).start();
        return START_STICKY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //stop music
        mp.pause();
    }

    class Play implements Runnable {
        @Override
        public void run() {

            mp = MediaPlayer.create(MusicService.this.getApplicationContext(), R.raw.single);
            mp.setLooping(true);
            mp.start();
        }

    }
}
