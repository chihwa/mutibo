package com.mutibo.austin.client;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
    }

    //click submit or next
    public void submitClicked(View view) {
        String question = ((TextView) findViewById(R.id.question)).getText().toString();
        String choice1 = ((TextView) findViewById(R.id.choice_1)).getText().toString();
        String choice2 = ((TextView) findViewById(R.id.choice_2)).getText().toString();
        String choice3 = ((TextView) findViewById(R.id.choice_3)).getText().toString();
        String choice4 = ((TextView) findViewById(R.id.choice_4)).getText().toString();
        String answer = ((TextView) findViewById(R.id.answer)).getText().toString();
        String explain = ((TextView) findViewById(R.id.explain)).getText().toString();


        if(null==question || null==choice1 || null==choice2 || null==choice3 || null==choice4 || null==answer || null==explain ||
         question.isEmpty() || choice1.isEmpty() || choice2.isEmpty() || choice3.isEmpty() || choice4.isEmpty() || answer.isEmpty() || explain.isEmpty()){
            Log.d("AddActivity", "not all the fields are populated");
            return;
        }


        String[] strArray = new String[]{choice1, choice2, choice3, choice4};
        String choices = StringUtils.join(strArray, ';');

        final MutiboSet mset = new MutiboSetBuilder().setQuestion(question).setChoice(choices).setAnswer(Integer.valueOf(answer)-1).setExplain(explain).createMutiboSet();

        String user = MutiboSvc.user;
        String pass = MutiboSvc.pass;


        final MutiboSetAPI svc = MutiboSvc.init(user, pass);

        new Thread(new Runnable() {
            public void run() {
                try {
                    svc.addSet(mset, new Callback<MutiboSet>() {
                        @Override
                        public void success(final MutiboSet mutiboSet, Response response) {
                            Log.d("addedSet=", mutiboSet.toString());
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            "New set added on server with id: " + mutiboSet.getId(),
                                            Toast.LENGTH_SHORT).show();
                                }
                            });

                            //clean up fields when successfully uploaded
                            ((TextView) findViewById(R.id.question)).setText("");
                            ((TextView) findViewById(R.id.choice_1)).setText("");
                            ((TextView) findViewById(R.id.choice_2)).setText("");
                            ((TextView) findViewById(R.id.choice_3)).setText("");
                            ((TextView) findViewById(R.id.choice_4)).setText("");
                            ((TextView) findViewById(R.id.answer)).setText("");
                            ((TextView) findViewById(R.id.explain)).setText("");
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("addSet failed=", error.toString());

                        }
                    });
                } catch (Exception e) {
                    Log.e("AddActivity", "Auth failure with Exception");

                }
            }
        }).start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
