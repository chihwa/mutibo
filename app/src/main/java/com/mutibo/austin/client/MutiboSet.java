package com.mutibo.austin.client;

public class MutiboSet {

	private String id;
	private String question;
	private String choice;
	private int answer;
	private String explain;
	private int presentedCnt;
	private int issueCnt;


    public MutiboSet(){}

    public MutiboSet(String id, String question, String choice, int answer, String explain, int presentedCnt, int issueCnt) {
        this.id = id;
        this.question = question;
        this.choice = choice;
        this.answer = answer;
        this.explain = explain;
        this.presentedCnt = presentedCnt;
        this.issueCnt = issueCnt;
    }

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	public int getAnswer() {
		return answer;
	}

	public void setAnswer(int answer) {
		this.answer = answer;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public int getPresentedCnt() {
		return presentedCnt;
	}

	public void setPresentedCnt(int presentedCnt) {
		this.presentedCnt = presentedCnt;
	}

	public int getIssueCnt() {
		return issueCnt;
	}

	public void setIssueCnt(int issueCnt) {
		this.issueCnt = issueCnt;
	}

    @Override
    public String toString(){
        return new String("Set ID="+getId()+" Question="+getQuestion());
    }

}
